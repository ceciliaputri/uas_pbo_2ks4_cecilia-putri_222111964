/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package siasik.entity;

import java.sql.Date;

//import java.util.Date;

//import java.time.Date;

/**
 *
 * @author ACER
 */
public class Peminjaman {
    private int idPeminjaman;
    private Pengguna pengguna;    
    private AlatMusik alatMusik;
    private String tujuan;  
    private Date tanggalPinjam;      
    private Date tanggalKembali;
    private int lamaPinjam;
    private String status;


    public Pengguna getPengguna() {
        return pengguna;
    }

    public void setPengguna(Pengguna pengguna) {
        this.pengguna = pengguna;
    }

    public Date getTanggalKembali() {
        return tanggalKembali;
    }

    public void setTanggalKembali(Date tanggalKembali) {
        this.tanggalKembali = tanggalKembali;
    }

    public Date getTanggalPinjam() {
        return tanggalPinjam;
    }

    public void setTanggalPinjam(Date tanggalPinjam) {
        this.tanggalPinjam = tanggalPinjam;
    }

    public String getStatus() {
        return status;
    }

    public AlatMusik getAlatMusik() {
        return alatMusik;
    }

    public void setAlatMusik(AlatMusik alatMusik) {
        this.alatMusik = alatMusik;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getIdPeminjaman() {
        return idPeminjaman;
    }

    public void setIdPeminjaman(int idPeminjaman) {
        this.idPeminjaman = idPeminjaman;
    }

    public String getTujuan() {
        return tujuan;
    }

    public void setTujuan(String tujuan) {
        this.tujuan = tujuan;
    }

    public int getLamaPinjam() {
        return lamaPinjam;
    }

    public void setLamaPinjam( int lamaPinjam) {
        this.lamaPinjam = lamaPinjam;
    }


}
