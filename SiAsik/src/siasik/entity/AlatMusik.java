/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package siasik.entity;

/**
 *
 * @author ACER
 */
public class AlatMusik {
    private int idAlatMusik;
    private String jenis;
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getIdAlatMusik() {
        return idAlatMusik;
    }

    public void setIdAlatMusik(int idAlatMusik) {
        this.idAlatMusik = idAlatMusik;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }   
}
