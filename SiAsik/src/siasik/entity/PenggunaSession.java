/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package siasik.entity;

/**
 *
 * @author ACER
 */
public class PenggunaSession {
    private static int id;
    private static String name;

    public static String getName() {
        return name;
    }

    public static void setName(String name) {
        PenggunaSession.name = name;
    }
    public static int getId() {
        return id;
    }

    public static void setId(int id) {
        PenggunaSession.id = id;
    }
}
