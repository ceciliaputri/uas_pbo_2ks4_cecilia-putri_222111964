/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package siasik.controller;

import java.util.logging.Logger;
import siasik.entity.Pengguna;
import siasik.repository.JDBCPenggunaRepository;
import siasik.repository.PenggunaRepository;

/**
 *
 * @author ACER
 */
public class RegistrasiController {
    private static final Logger logger = Logger.getLogger(RegistrasiController.class.getName());

    public boolean validateIsian(String nama, String kelas, String email, String password) {
        return (nama.isEmpty() || kelas.isEmpty() || email.isEmpty() || password.isEmpty());
        
    }
    
    public boolean validateName(String name) {
        // Nama harus terdiri dari huruf dengan panjang minimal 3 karakter
        return name.matches("^[a-zA-Z]{3,}$");
    }

    public boolean validateClass(String cls) {
        // Kelas harus terdiri dari huruf dan angka dengan panjang minimal 1 karakter
        return cls.matches("^[a-zA-Z0-9]{1,}$");
    }

    public boolean validateEmail(String email) {
        // Validasi menggunakan regular expression untuk format email
        return email.matches("^[A-Za-z0-9+_.-]+@[A-Za-z0-9.-]+$");
    }

    public boolean validatePassword(String password) {
        // Password harus memiliki panjang minimal 5 karakter 
        return password.length() >= 5;
    }
    
     public String regist(String nama, String kelas, String email, String password){

        try {
            Pengguna pengguna = new Pengguna();
            PenggunaRepository penggunaRepo = new JDBCPenggunaRepository();
            if (penggunaRepo.getPenggunaByEmail(email).getNama()!= null) {
                return "Email telah digunakan!";
            }
            pengguna.setNama(nama);
            pengguna.setKelas(kelas);
            pengguna.setEmail(email);
            pengguna.setPassword(password);
            penggunaRepo.insertPengguna(pengguna);
            return "Berhasil Melakukan Registrasi, Silakan Login terlebih dahulu!";
        } catch (Exception ex) {
            logger.log(java.util.logging.Level.SEVERE, "Failed to insert Pengguna", ex);
//            JOptionPane.showMessageDialog(this, "gagal mengambil data.", "Error", JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace();
        }
        return "Gagal Registrasi, terjadi kesalahan";
    } 
}
