/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package siasik.controller;

import java.util.logging.Logger;
import siasik.entity.Pengguna;
import siasik.entity.PenggunaSession;
import siasik.repository.JDBCPenggunaRepository;
import siasik.repository.PenggunaRepository;

/**
 *
 * @author ACER
 */
public class LoginController {
    private static final Logger logger = Logger.getLogger(LoginController.class.getName());
    
    public boolean validateIsiKolom(String email, String password){
        return (email.isEmpty() || password.isEmpty());
    }
       
    public String login(String email, String password){
        try{
            PenggunaRepository penggunaRepo = new JDBCPenggunaRepository();
            Pengguna pengguna = penggunaRepo.getPenggunaByEmail(email);
            if (email.equals("admin@gmail.com")){
                if (password.equals("admin")) {
                    return "Selamat Datang Admin!";
                    
                }else{
                    return "Password salah!";
                }
            }else if (email.equals(pengguna.getEmail())){
                if (pengguna.getPassword().equals(password)) {
                    PenggunaSession.setId(pengguna.getIdPengguna());
                    PenggunaSession.setName(pengguna.getNama());
                    return "Login Berhasil";
                }else{
                    return "Login Gagal, password tidak sesuai";
                }
            }else{
                return "Akunmu Belum ada, silakan Registrasi dahulu";
            }  
        }catch (Exception ex) {
            logger.log(java.util.logging.Level.SEVERE, "Failed to Login", ex);
            ex.printStackTrace();
    }
      return "Gagal Login, tidak terhubung";  
    } 
    
}

