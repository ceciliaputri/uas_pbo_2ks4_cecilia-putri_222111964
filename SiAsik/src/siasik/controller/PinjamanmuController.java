/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package siasik.controller;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
import siasik.view.Pinjamanmu;
import siasik.entity.Peminjaman;
import siasik.entity.PenggunaSession;
import siasik.repository.JDBCPeminjamanRepository;
import siasik.repository.PeminjamanRepository;

/**
 *
 * @author ACER
 */
public class PinjamanmuController {
    private static final Logger logger = Logger.getLogger(Pinjamanmu.class.getName());
    private final int idPengguna = PenggunaSession.getId();
    private List<Peminjaman> dipinjam = new ArrayList<>();
    private String status;
    Date tanggalSekarang = new Date(System.currentTimeMillis());
    
     public void loadTable(DefaultTableModel dtm){
        //refresh table
        while(dtm.getRowCount()>0){
            dtm.removeRow(0);
        }
        try {
            PeminjamanRepository peminjamanRepo = new JDBCPeminjamanRepository();
            List<Peminjaman> peminjamanList = peminjamanRepo.getListPeminjamanByIdPengguna(idPengguna);
//            int i=1;
            for (int i = 0; i < peminjamanList.size(); i++) {
                Peminjaman peminjaman = peminjamanList.get(i);
                
                if (peminjaman.getStatus().equals("Belum Diambil") || peminjaman.getStatus().equals("Dipinjam")) {
                    if (peminjaman.getStatus().equals("Dipinjam"))
                        status = "Belum Dikembalikan";
                    else
                        status = "Belum Diambil";
                    dtm.addRow(new Object[]{peminjaman.getAlatMusik().getJenis(), peminjaman.getTanggalPinjam(), peminjaman.getTanggalKembali(),status});
                    dipinjam.add(peminjaman);
                }
            }
        }catch (Exception ex) {       
            logger.log(java.util.logging.Level.SEVERE, "Failed to save data to database", ex);
//            JOptionPane.showMessageDialog(this, "gagal mengambil data.", "Error", JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace();
            }  
    }
     public String updateStatusKembalikan(DefaultTableModel dtm) {
        String status = "Dikembalikan"; 
        int rowCount = dtm.getRowCount();
        
        for (int row = 0; row < rowCount; row++) {
            Object value = dtm.getValueAt(row, 4);
            
            if (value != null && (boolean) value) {
                try {
                    PeminjamanRepository peminjamanRepository = new JDBCPeminjamanRepository();
                    
                    Peminjaman peminjaman = dipinjam.get(row); // Mengambil objek Peminjaman dari peminjamanList
                    if (peminjaman.getStatus().equals("Dipinjam")) {
                        int idPeminjaman = peminjaman.getIdPeminjaman(); // Mendapatkan ID dari objek Peminjaman
                        // Melakukan pembaruan status peminjaman menggunakan idPeminjaman
                        peminjamanRepository.updateStatusPeminjaman(idPeminjaman, status);
                        return "Terimakasih telah Melakukan Peminjaman dengan SiAsik";
                    }else
                        return "Alat musik belum diambil";
                } catch (Exception ex) {
                    logger.log(Level.SEVERE, "Failed to save data to database", ex);
//                    JOptionPane.showMessageDialog(this, "gagal meng-update data.", "Error", JOptionPane.ERROR_MESSAGE);
                    ex.printStackTrace();
                    }
                }
            }
            loadTable(dtm);
            System.out.println("data diupdate");
            return "Gagal menyambungkan database";
        }
     public String updateStatusAmbil(DefaultTableModel dtm) {
        String status = "Dipinjam"; 
        int rowCount = dtm.getRowCount();

        for (int row = 0; row < rowCount; row++) {
//          int idPeminjaman = (int) dtm.getValueAt(row, 0);
            Object value = dtm.getValueAt(row, 4);
            
            if (value != null && (boolean) value) {
                try {
                    PeminjamanRepository peminjamanRepository = new JDBCPeminjamanRepository();
                    Peminjaman peminjaman = dipinjam.get(row); // Mengambil objek Peminjaman dari peminjamanList
                    if (peminjaman.getStatus().equals("Belum Diambil")) {
                        if (peminjaman.getTanggalPinjam().after(tanggalSekarang)) {
                            return "Alat Musik belum bisa diambil";
                        }
                        int idPeminjaman = peminjaman.getIdPeminjaman(); // Mendapatkan ID dari objek Peminjaman
                        // Melakukan pembaruan status peminjaman menggunakan idPeminjaman
                        peminjamanRepository.updateStatusPeminjaman(idPeminjaman, status);
                        return "Alat musik diambil";
                    }
                } catch (Exception ex) {
                    logger.log(Level.SEVERE, "Failed to save data to database", ex);
//                    JOptionPane.showMessageDialog(this, "gagal meng-update data.", "Error", JOptionPane.ERROR_MESSAGE);
                    ex.printStackTrace();
                    }
                }
            }
            System.out.println("data diupdate");
            return "Alat Musik sudah anda ambil sebelumnya!";
        }
     
     
     public boolean validateUpdate(DefaultTableModel dtm){
        int rowCount = dtm.getRowCount();
        boolean cek =false;
        for (int row = 0; row < rowCount; row++) {
//          int idPeminjaman = (int) dtm.getValueAt(row, 0);
            Object value = dtm.getValueAt(row, 4);
            if (value != null ){
                cek = true;
                return cek;
            }
        }
        return cek;
}
}
