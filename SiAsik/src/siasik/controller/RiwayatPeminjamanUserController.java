/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package siasik.controller;

import java.util.List;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
import siasik.entity.Peminjaman;
import siasik.entity.PenggunaSession;
import siasik.repository.JDBCPeminjamanRepository;
import siasik.repository.PeminjamanRepository;

/**
 *
 * @author ACER
 */
public class RiwayatPeminjamanUserController {
    private static final Logger logger = Logger.getLogger(CekAlatMusikController.class.getName());
    private PenggunaSession pengguna;
     public void loadTable(DefaultTableModel dtm){
        //refresh table
        while(dtm.getRowCount()>0){
            dtm.removeRow(0);
        }
        try {
            PeminjamanRepository peminjamanRepository = new JDBCPeminjamanRepository();
            List<Peminjaman> peminjamanList = peminjamanRepository.getListPeminjamanByIdPengguna(PenggunaSession.getId());
            int i=1;
            for(Peminjaman peminjaman: peminjamanList){ 
                if (peminjaman.getStatus().equals("Dipinjam")){
                    continue;
                }
                else
                    dtm.addRow(new Object[]{ i,peminjaman.getAlatMusik().getJenis(), peminjaman.getTanggalPinjam(), peminjaman.getTanggalKembali(),peminjaman.getTujuan(), peminjaman.getStatus()});
                    i++;
            }
        }catch (Exception ex) {       
            logger.log(java.util.logging.Level.SEVERE, "Failed to save data to database", ex);
//            JOptionPane.showMessageDialog(this, "gagal mengambil data.", "Error", JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace();
            }       
    }
}
