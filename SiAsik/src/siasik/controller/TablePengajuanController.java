/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package siasik.controller;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
import siasik.view.TabelPengajuan;
import siasik.entity.Peminjaman;
import siasik.repository.JDBCPeminjamanRepository;
import siasik.repository.PeminjamanRepository;

/**
 *
 * @author ACER
 */
public class TablePengajuanController {
    private static final Logger logger = Logger.getLogger(TabelPengajuan.class.getName());
    
    public void loadTableData (DefaultTableModel dtm){
      
        //refresh table
        while(dtm.getRowCount()>0){
            dtm.removeRow(0);
        }
        try {
            PeminjamanRepository peminjamanRepository = new JDBCPeminjamanRepository();
            List<Peminjaman> peminjamanList = peminjamanRepository.getListPeminjaman();
            int i=1;
            for(Peminjaman peminjaman: peminjamanList){ 
                if (peminjaman.getStatus().equals("Diajukan")){
                    dtm.addRow(new Object[]{peminjaman.getIdPeminjaman(), peminjaman.getPengguna().getNama(),peminjaman.getPengguna().getEmail(),peminjaman.getAlatMusik().getJenis(), peminjaman.getTanggalPinjam(), peminjaman.getTanggalKembali(),peminjaman.getTujuan()});
                    i++;
                }     
            }
        }catch (Exception ex) {
            logger.log(Level.SEVERE, "Failed to load table", ex);
//            JOptionPane.showMessageDialog(this, "gagal mengambil data.", "Error", JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace();
        }       
    }
    
    public void updateStatus(String status, DefaultTableModel dtm) {
        
        String statusAmbil = "Belum Diambil";
        int rowCount = dtm.getRowCount();
        for (int row = 0; row < rowCount; row++) {
            int idPeminjaman = (int) dtm.getValueAt(row, 0);
            Object value = dtm.getValueAt(row, 7);

            if (value != null && (boolean) value) {
                try {
                    PeminjamanRepository peminjamanRepository = new JDBCPeminjamanRepository();
                    if(status.equals("Disetujui"))
                        peminjamanRepository.updateStatusPeminjaman(idPeminjaman, statusAmbil);
                    else
                        peminjamanRepository.updateStatusPeminjaman(idPeminjaman, status);
                        
                } catch (Exception ex) {
                    logger.log(Level.SEVERE, "Failed to save data to database", ex);
//                    JOptionPane.showMessageDialog(this, "gagal meng-update data.", "Error", JOptionPane.ERROR_MESSAGE);
                    ex.printStackTrace();
                    }
                }
            }
        }
}
