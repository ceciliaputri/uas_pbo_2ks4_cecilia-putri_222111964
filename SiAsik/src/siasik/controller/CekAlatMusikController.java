/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package siasik.controller;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
import org.apache.commons.lang3.time.DateUtils;
import siasik.entity.Peminjaman;
import siasik.repository.JDBCPeminjamanRepository;
import siasik.repository.PeminjamanRepository;
import siasik.view.Pinjamanmu;


/**
 *
 * @author ACER
 */
public class CekAlatMusikController {
    private static final Logger logger = Logger.getLogger(CekAlatMusikController.class.getName());
//    private static final Logger logger = Logger.getLogger(Pinjamanmu.class.getName());
    
    public boolean validateIsiTanggal(LocalDate tanggalPinjam, LocalDate tanggalKembali){
        return (tanggalPinjam == null || tanggalKembali == null);
    }
    
    public boolean validateTanggalSekarang(Date pinjam){
        Date tanggalSekarang = new Date(System.currentTimeMillis());
        if (DateUtils.isSameDay(pinjam, tanggalSekarang)) {
            return false; // Mengembalikan false jika pinjam adalah tanggal hari ini
        }
        return (pinjam.before(tanggalSekarang));
    }
    
     public void loadTable(DefaultTableModel dtm){
        //refresh table
        while(dtm.getRowCount()>0){
            dtm.removeRow(0);
        }
        try {
            PeminjamanRepository peminjamanRepository = new JDBCPeminjamanRepository();
            List<Peminjaman> peminjamanList = peminjamanRepository.getListPeminjaman();
            int i=1;
            for(Peminjaman peminjaman: peminjamanList){ 
                if (peminjaman.getStatus().equals("Dipinjam") || peminjaman.getStatus().equals("Belum Diambil")){
                    dtm.addRow(new Object[]{ i,peminjaman.getPengguna().getNama(),peminjaman.getAlatMusik().getJenis(), peminjaman.getTanggalPinjam(), peminjaman.getTanggalKembali(),peminjaman.getStatus()});
                    i++;
                }
                
            }
        }catch (Exception ex) {       
            logger.log(java.util.logging.Level.SEVERE, "Failed to save data to database", ex);
//            JOptionPane.showMessageDialog(this, "gagal mengambil data.", "Error", JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace();
            }       
    }
     
    private int getSelectedIdAlatMusik(String jenisAlat){
        int idAlatMusik = 0;
        switch (jenisAlat){
            case "Gitar Listrik":
                idAlatMusik = 1;
                break;
            case "Gitar Akustik":
                idAlatMusik = 2;
                break;
            case "Bass":
                idAlatMusik = 3;
                break; 
            case "Keyboard":
                idAlatMusik = 4;
                break;
            case "Cajon":
                idAlatMusik = 5;
                break; 
            case "Biola":
                idAlatMusik = 6;
                break; 
            case "Biola Contrabass":
                idAlatMusik = 7;
                break; 
            case "Cello":
                idAlatMusik = 8;
                break; 
            case "Drum":
                idAlatMusik = 9;
                break;                 
        }
        return idAlatMusik;
    }
    
    public boolean cekAlatMusik(String jenisAlat, Date tanggalPinjam, Date tanggalKembali){
        try {
            PeminjamanRepository peminjamanRepo = new JDBCPeminjamanRepository();
            int idJenisAlat = getSelectedIdAlatMusik(jenisAlat);
            return(peminjamanRepo.getPeminjamanByDate(idJenisAlat, tanggalPinjam, tanggalKembali));
            
        } catch (Exception ex) {
            logger.log(java.util.logging.Level.SEVERE, "Failed to check availability", ex);
//            JOptionPane.showMessageDialog(this, "gagal mengambil data.", "Error", JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace();
        }
        return false;
    }  
    
   
}
