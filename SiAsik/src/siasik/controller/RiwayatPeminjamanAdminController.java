/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package siasik.controller;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
import siasik.view.TabelPengajuan;
import siasik.entity.Peminjaman;
import siasik.repository.JDBCPeminjamanRepository;
import siasik.repository.PeminjamanRepository;

/**
 *
 * @author ACER
 */
public class RiwayatPeminjamanAdminController {
    private static final Logger logger = Logger.getLogger(RiwayatPeminjamanAdminController.class.getName());
    
    public void loadTableData (DefaultTableModel dtm){
      
        //refresh table
        while(dtm.getRowCount()>0){
            dtm.removeRow(0);
        }
        try {
            PeminjamanRepository peminjamanRepository = new JDBCPeminjamanRepository();
            List<Peminjaman> peminjamanList = peminjamanRepository.getListPeminjaman();
            int i=1;
            for(Peminjaman peminjaman: peminjamanList){ 
                if (peminjaman.getStatus().equals("Dikembalikan")){
                    dtm.addRow(new Object[]{peminjaman.getPengguna().getNama(),peminjaman.getAlatMusik().getJenis(), peminjaman.getTanggalPinjam(), peminjaman.getTanggalKembali(),peminjaman.getTujuan()});
                    i++;
                }     
            }
            System.out.println("load table data selese");
        }catch (Exception ex) {
            logger.log(Level.SEVERE, "Failed to load table", ex);
//            JOptionPane.showMessageDialog(this, "gagal mengambil data.", "Error", JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace();
        }       
    }
    
}