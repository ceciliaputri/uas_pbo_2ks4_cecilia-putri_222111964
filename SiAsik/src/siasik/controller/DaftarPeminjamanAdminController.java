/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package siasik.controller;

import java.util.List;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
import siasik.entity.Peminjaman;
import siasik.repository.JDBCPeminjamanRepository;
import siasik.repository.PeminjamanRepository;
import siasik.view.Pinjamanmu;

/**
 *
 * @author ACER
 */
public class DaftarPeminjamanAdminController {
 private static final Logger logger = Logger.getLogger(DaftarPeminjamanAdminController.class.getName());
    
     public void loadTable(DefaultTableModel dtm){
        //refresh table
        while(dtm.getRowCount()>0){
            dtm.removeRow(0);
        }
        try {
            PeminjamanRepository peminjamanRepository = new JDBCPeminjamanRepository();
            List<Peminjaman> peminjamanList = peminjamanRepository.getListPeminjaman();
            int i=1;
            String status = "Belum Dikembalikan";
            for(Peminjaman peminjaman: peminjamanList){ 
                if (peminjaman.getStatus().equals("Dipinjam")){
                    dtm.addRow(new Object[]{ i,peminjaman.getPengguna().getNama(),peminjaman.getAlatMusik().getJenis(), peminjaman.getTanggalPinjam(), peminjaman.getTanggalKembali(),status});
                    i++;
                }else if(peminjaman.getStatus().equals("Belum Diambil")){
                    dtm.addRow(new Object[]{ i,peminjaman.getPengguna().getNama(),peminjaman.getAlatMusik().getJenis(), peminjaman.getTanggalPinjam(), peminjaman.getTanggalKembali(),peminjaman.getStatus()});
                    i++;
                }
                
            }
        }catch (Exception ex) {       
            logger.log(java.util.logging.Level.SEVERE, "Failed to get daftar peminjaman admin", ex);
//            JOptionPane.showMessageDialog(this, "gagal mengambil data.", "Error", JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace();
            }       
    }
}
