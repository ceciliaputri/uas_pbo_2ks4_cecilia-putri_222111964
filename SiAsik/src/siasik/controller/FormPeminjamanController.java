/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package siasik.controller;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
//import java.time.Date;
//import java.time.ZoneId;
import org.apache.commons.lang3.time.DateUtils;
import java.util.logging.Logger;
import javax.swing.JScrollPane;

import siasik.view.FormPeminjaman;
import siasik.entity.AlatMusik;
import siasik.entity.Peminjaman;
import siasik.entity.Pengguna;
import siasik.entity.PenggunaSession;
import siasik.repository.AlatMusikRepository;
import siasik.repository.JDBCAlatMusikRepository;
import siasik.repository.JDBCPeminjamanRepository;
import siasik.repository.JDBCPenggunaRepository;
import siasik.repository.PeminjamanRepository;
import siasik.repository.PenggunaRepository;

/**
 *
 * @author ACER
 */
public class FormPeminjamanController{
    private static final Logger logger = Logger.getLogger(FormPeminjamanController.class.getName());
   
   
    public Pengguna loadForm(){
        PenggunaRepository penggunaRepo = new JDBCPenggunaRepository(); 
        Pengguna pengguna = penggunaRepo.getPenggunaById(PenggunaSession.getId());
        return pengguna;
    }

    public boolean validateIsiKolom(LocalDate tanggalPinjam, LocalDate tanggalKembali, String tujuan){
        return (tanggalPinjam == null || tanggalKembali == null || tujuan.isEmpty());
    }
    
    public boolean validateTanggalSekarang(Date pinjam){
        Date tanggalSekarang = new Date(System.currentTimeMillis());
        if (DateUtils.isSameDay(pinjam, tanggalSekarang)) {
            return false; // Mengembalikan false jika pinjam adalah tanggal hari ini
        }
        return (pinjam.before(tanggalSekarang));
    
    }
    
    public boolean validateIsiTanggal(LocalDate tanggalPinjam, LocalDate tanggalKembali){
        return (tanggalPinjam != null && tanggalKembali != null);
    }
    
    private int getSelectedIdAlatMusik(String jenisAlat){
        int idAlatMusik = 0;
        switch (jenisAlat){
            case "Gitar Listrik":
                idAlatMusik = 1;
                break;
            case "Gitar Akustik":
                idAlatMusik = 2;
                break;
            case "Bass":
                idAlatMusik = 3;
                break; 
            case "Keyboard":
                idAlatMusik = 4;
                break;
            case "Cajon":
                idAlatMusik = 5;
                break; 
            case "Biola":
                idAlatMusik = 6;
                break; 
            case "Biola Contrabass":
                idAlatMusik = 7;
                break; 
            case "Cello":
                idAlatMusik = 8;
                break; 
            case "Drum":
                idAlatMusik = 9;
                break;                 
        }
        return idAlatMusik;
    }
    
    public int lamaPinjam(Date tanggalPinjam, Date tanggalKembali){
        long tanggal1 = tanggalPinjam.getTime();
        long tanggal2 = tanggalKembali.getTime();
        System.out.println("ini tanggal");
        System.out.println(tanggal1);
        System.out.println(tanggal2);
        
        // Hitung selisih hari
        int lamaHari = (int) (tanggal2- tanggal1 + 86400000 )/ (24 * 60 * 60 * 1000); //rumus ngitung lama harinya
        return (int)lamaHari;
    }


    
    public boolean cekAlatMusik(String jenisAlat, Date tanggalPinjam, Date tanggalKembali){
        try {
            PeminjamanRepository peminjamanRepo = new JDBCPeminjamanRepository();
            int idJenisAlat = getSelectedIdAlatMusik(jenisAlat);
            return(peminjamanRepo.getPeminjamanByDate(idJenisAlat, tanggalPinjam, tanggalKembali));
            
        } catch (Exception ex) {
            logger.log(java.util.logging.Level.SEVERE, "Failed to check availability", ex);
//            JOptionPane.showMessageDialog(this, "gagal mengambil data.", "Error", JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace();
        }
        return false;
    } 
    public void ajukanPinjaman(String jenisAlat, String tujuan, Date tanggalPinjam, Date tanggalKembali, int lamaPinjam){
        try {
            
            Peminjaman peminjaman = new Peminjaman();
            PeminjamanRepository peminjamanRepo = new JDBCPeminjamanRepository();
            AlatMusikRepository alatMusikRepo = new JDBCAlatMusikRepository();
            PenggunaRepository penggunaRepo = new JDBCPenggunaRepository();

            AlatMusik alatMusik = alatMusikRepo.getAlatMusikById(getSelectedIdAlatMusik(jenisAlat));
            Pengguna pengguna = penggunaRepo.getPenggunaById(PenggunaSession.getId()); 
            peminjaman.setPengguna(pengguna);
            peminjaman.setAlatMusik(alatMusik);
            peminjaman.setTujuan(tujuan);
            peminjaman.setTanggalPinjam(tanggalPinjam);
            peminjaman.setTanggalKembali(tanggalKembali);
            peminjaman.setLamaPinjam(lamaPinjam);
            peminjaman.setStatus("Diajukan");            
            peminjamanRepo.insertPeminjaman(peminjaman);
        } catch (Exception ex) {
            logger.log(java.util.logging.Level.SEVERE, "Failed to ajukan peminjaman", ex);
//            JOptionPane.showMessageDialog(this, "gagal mengambil data.", "Error", JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace();
        }
    }  
}

