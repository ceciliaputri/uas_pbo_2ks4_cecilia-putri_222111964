/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package siasik.repository;

import java.util.List;
import siasik.entity.AlatMusik;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
//import java.sql.Statement;
/**
 *
 * @author ACER
 */
public class JDBCAlatMusikRepository implements AlatMusikRepository{
    private Connection connection;
    
    public JDBCAlatMusikRepository() {
        connection = DatabaseConnection.getConnection();
    }

    @Override
    public List<AlatMusik> getListAlatMusik() {
     List<AlatMusik> musikList = new ArrayList<>();
        try {
            PreparedStatement preparedStatement1 = connection.prepareStatement("SELECT * FROM alatmusik");
            ResultSet resultSet = preparedStatement1.executeQuery();
            while (resultSet.next()) {
                AlatMusik alatMusik = new AlatMusik();
                alatMusik.setIdAlatMusik(resultSet.getInt("id"));                
                alatMusik.setJenis(resultSet.getString("jenis"));
                alatMusik.setStatus(resultSet.getString("status"));
                musikList.add(alatMusik);
            }
        } catch (Exception e) {
        }
        return musikList;
    }

    @Override
    public AlatMusik getAlatMusikById(int id) {
        AlatMusik alatMusik = new AlatMusik();
        try {
            PreparedStatement preparedStatementAlatMusik = connection.prepareStatement("SELECT * FROM alatmusik WHERE id = ?");            
            preparedStatementAlatMusik.setInt(1, id);
            ResultSet resultSetAlatMusik = preparedStatementAlatMusik.executeQuery();
            
            while (resultSetAlatMusik.next()) {
                alatMusik.setJenis(resultSetAlatMusik.getString("jenis"));
                alatMusik.setIdAlatMusik(resultSetAlatMusik.getInt("id"));                
                alatMusik.setStatus(resultSetAlatMusik.getString("status"));      
            }
            
        } catch (Exception e) {
        }
        return alatMusik;    
    }

}
