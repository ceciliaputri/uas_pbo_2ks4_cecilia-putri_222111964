/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package siasik.repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import siasik.entity.AlatMusik;
import siasik.entity.Peminjaman;

/**
 *
 * @author ACER
 */
public class JDBCPeminjamanRepository implements PeminjamanRepository{
    private Connection connection;
    public JDBCPeminjamanRepository() {
        connection = DatabaseConnection.getConnection();
    }

    @Override
    public List<Peminjaman> getListPeminjaman() {
       List<Peminjaman> peminjamanList = new ArrayList<>();
       PenggunaRepository penggunaRepo = new JDBCPenggunaRepository(); 
       AlatMusikRepository alatRepo = new JDBCAlatMusikRepository(); 
        try {
            PreparedStatement preparedStatement1 = connection.prepareStatement("SELECT * FROM peminjaman");
            ResultSet resultSet = preparedStatement1.executeQuery();
            while (resultSet.next()) {
                Peminjaman peminjaman = new Peminjaman();
                peminjaman.setIdPeminjaman(resultSet.getInt("id"));
                peminjaman.setPengguna(penggunaRepo.getPenggunaById(resultSet.getInt("idPengguna")));
                peminjaman.setAlatMusik(alatRepo.getAlatMusikById(resultSet.getInt("idAlatMusik")));
                peminjaman.setTujuan(resultSet.getString("tujuan"));
                peminjaman.setTanggalPinjam(resultSet.getDate("tanggalPinjam"));                  
                peminjaman.setTanggalKembali(resultSet.getDate("tanggalKembali"));  
                peminjaman.setStatus(resultSet.getString("status"));
                peminjamanList.add(peminjaman);
            }
        } catch (Exception e) {
        }
        return peminjamanList; 
    }

    
    @Override
    public void insertPeminjaman(Peminjaman peminjaman) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO peminjaman (idPengguna, tujuan, tanggalPinjam, tanggalKembali,idAlatMusik, status)VALUES (?,?, ?, ?, ?, ?)");
            preparedStatement.setInt(1,peminjaman.getPengguna().getIdPengguna());            
            preparedStatement.setString(2, peminjaman.getTujuan());
            preparedStatement.setDate(3, peminjaman.getTanggalPinjam());
            preparedStatement.setDate(4, peminjaman.getTanggalKembali());
            preparedStatement.setInt(5, peminjaman.getAlatMusik().getIdAlatMusik());
            preparedStatement.setString(6, peminjaman.getStatus());
            preparedStatement.executeUpdate();
        } catch (SQLException e){
            e.printStackTrace();    
        }   
    }

    @Override
    public void updateStatusPeminjaman(int idPeminjaman, String status) {
        try {
        PreparedStatement preparedStatement = connection.prepareStatement("UPDATE peminjaman SET status = ? WHERE id = ?");
        preparedStatement.setString(1, status);
        preparedStatement.setInt(2, idPeminjaman);
        preparedStatement.executeUpdate();
            System.out.println("berhasil diupdate");
    } catch (SQLException e) {
        e.printStackTrace();
    }
    }

    @Override
    public Peminjaman getPeminjamanById(int id) {
        Peminjaman peminjaman = new Peminjaman();
        PenggunaRepository penggunaRepo = new JDBCPenggunaRepository(); 
        AlatMusikRepository alatRepo = new JDBCAlatMusikRepository(); 
   
        try {
            PreparedStatement preparedStatementPeminjaman = connection.prepareStatement("SELECT * FROM peminjaman WHERE id = ?");            
            preparedStatementPeminjaman.setInt(1, id);
            ResultSet resultSet = preparedStatementPeminjaman.executeQuery();
            
            while (resultSet.next()) {
                peminjaman.setIdPeminjaman(resultSet.getInt("id"));
                peminjaman.setPengguna(penggunaRepo.getPenggunaById(resultSet.getInt("idPengguna")));
                peminjaman.setAlatMusik(alatRepo.getAlatMusikById(resultSet.getInt("idAlatMusik")));
                peminjaman.setTujuan(resultSet.getString("tujuan"));
                peminjaman.setTanggalPinjam(resultSet.getDate("tanggalPinjam"));                  
                peminjaman.setTanggalKembali(resultSet.getDate("tanggalKembali"));  
                peminjaman.setStatus(resultSet.getString("status"));
            }
            
        } catch (Exception e) {
        }
        return peminjaman;    
    }    

    @Override
    public List<Peminjaman> getListPeminjamanByIdPengguna(int idPengguna) {
        List<Peminjaman> peminjamanList = new ArrayList<>();
        PenggunaRepository penggunaRepo = new JDBCPenggunaRepository(); 
        AlatMusikRepository alatRepo = new JDBCAlatMusikRepository(); 
   
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM peminjaman WHERE idPengguna = ?");            
            preparedStatement.setInt(1, idPengguna);
            ResultSet resultSet = preparedStatement.executeQuery(); 
            while (resultSet.next()) {
                Peminjaman peminjaman = new Peminjaman();
                peminjaman.setIdPeminjaman(resultSet.getInt("id"));
                peminjaman.setPengguna(penggunaRepo.getPenggunaById(resultSet.getInt("idPengguna")));
                peminjaman.setAlatMusik(alatRepo.getAlatMusikById(resultSet.getInt("idAlatMusik")));
                peminjaman.setTujuan(resultSet.getString("tujuan"));
                peminjaman.setTanggalPinjam(resultSet.getDate("tanggalPinjam"));                  
                peminjaman.setTanggalKembali(resultSet.getDate("tanggalKembali"));  
                peminjaman.setStatus(resultSet.getString("status"));
                peminjamanList.add(peminjaman);
            }
        } catch (Exception e) {
        }
        return peminjamanList;    
    }

    @Override
    public boolean getPeminjamanByDate(int idAlatMusik, Date pinjam, Date kembali) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement
                           ("""
                            SELECT COUNT(*) AS jumlah_peminjaman
                            FROM peminjaman
                            WHERE tanggalPinjam <= ?
                              AND tanggalKembali >= ?
                              AND idAlatMusik = ? 
                              AND (status = ? OR status = ?) """);            
            preparedStatement.setDate(1, (java.sql.Date) kembali);
            preparedStatement.setDate(2, (java.sql.Date) pinjam);
            preparedStatement.setInt(3, idAlatMusik);
            preparedStatement.setString(4, "Dipinjam");
            preparedStatement.setString(5, "Belum Diambil");
            ResultSet resultSet = preparedStatement.executeQuery(); 
            if (resultSet.next()) {
                int tersedia = resultSet.getInt("jumlah_peminjaman");
                if(tersedia == 0)
                    return true;
            }
        } catch (Exception e) {
            System.err.println("Error while executing SQL query: " + e.getMessage());

        }
        return false; 
    }

    
}


    
