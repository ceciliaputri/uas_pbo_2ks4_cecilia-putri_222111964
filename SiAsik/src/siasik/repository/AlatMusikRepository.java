/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package siasik.repository;

import java.util.List;
import siasik.entity.AlatMusik;

/**
 *
 * @author ACER
 */
public interface AlatMusikRepository {
    List<AlatMusik> getListAlatMusik();
    AlatMusik getAlatMusikById(int id);
}
