/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package siasik.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ACER
 */
public class DatabaseConnection {
    private static final String URL = "jdbc:mysql://localhost:3306/siasik";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "";
    private static final Logger LOGGER = Logger.getLogger(DatabaseConnection.class.getName());
    private static Connection connection;

    public static Connection getConnection() {
        try {
            if(connection != null)
                return connection;
            else if (connection == null || connection.isClosed()) {
                LOGGER.log(Level.INFO, "Connecting to database...");
                Class.forName("com.mysql.cj.jdbc.Driver");
                connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
                LOGGER.log(Level.INFO, "Database connection established.");
            }
        } catch (ClassNotFoundException | SQLException e) {
            LOGGER.log(Level.SEVERE, "Error establishing database connection.", e);
        }
        return connection;
    }
}
