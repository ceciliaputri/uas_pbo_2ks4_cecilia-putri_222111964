/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package siasik.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import siasik.entity.Pengguna;
import siasik.entity.Pengguna;

/**
 *
 * @author ACER
 */
public class JDBCPenggunaRepository implements PenggunaRepository {
private Connection connection;
    public JDBCPenggunaRepository() {
        connection = DatabaseConnection.getConnection();
    }
    @Override
    public List<Pengguna> getListPengguna() {
        List<Pengguna> penggunaList = new ArrayList<>();
//       private  alatmusik;
        try {
            PreparedStatement preparedStatement1 = connection.prepareStatement("SELECT * FROM pengguna");
            ResultSet resultSet = preparedStatement1.executeQuery();
            while (resultSet.next()) { //peminjaman.get().getId()
                Pengguna pengguna = new Pengguna();
                pengguna.setIdPengguna(resultSet.getInt("id"));                
                pengguna.setNama(resultSet.getString("nama"));
                pengguna.setEmail(resultSet.getString("email"));
                pengguna.setPassword(resultSet.getString("password"));
                penggunaList.add(pengguna);
            }
        } catch (Exception e) {
        }
        return penggunaList; 
    }

    @Override
    public void insertPengguna(Pengguna pengguna) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO pengguna (id, nama, email, kelas, password)VALUES (?,?,?,?,?)");
            preparedStatement.setInt(1,0);
            preparedStatement.setString(2, pengguna.getNama());
            preparedStatement.setString(3, pengguna.getEmail());
            preparedStatement.setString(4, pengguna.getKelas());
            preparedStatement.setString(5, pengguna.getPassword());
            preparedStatement.executeUpdate();
        } catch (SQLException e){
            e.printStackTrace();    
        }       
    }

    @Override
    public Pengguna getPenggunaById(int idPengguna) {
        Pengguna pengguna = new Pengguna();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM pengguna WHERE id = ?");            
            preparedStatement.setInt(1, idPengguna);
            ResultSet resultSet = preparedStatement.executeQuery();
            
            while (resultSet.next()) {
                pengguna.setIdPengguna(resultSet.getInt("id"));
                pengguna.setNama(resultSet.getString("nama"));
                pengguna.setEmail(resultSet.getString("email"));                
                pengguna.setKelas(resultSet.getString("kelas"));      
                pengguna.setPassword(resultSet.getString("password"));      
            }
            
        } catch (Exception e) {
        }
        return pengguna;   
    }

    @Override
    public Pengguna getPenggunaByEmail(String email) {
        Pengguna pengguna = new Pengguna();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM pengguna WHERE email = ?");            
            preparedStatement.setString(1,email);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                pengguna.setIdPengguna(resultSet.getInt("id"));
                pengguna.setNama(resultSet.getString("nama"));
                pengguna.setEmail(resultSet.getString("email"));                
                pengguna.setKelas(resultSet.getString("kelas"));      
                pengguna.setPassword(resultSet.getString("password"));                         
            } 
              
        } catch (Exception e) {
        }
        return pengguna;   
    }
    
}
