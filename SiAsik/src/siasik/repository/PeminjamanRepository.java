/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package siasik.repository;

import java.util.Date;
import java.util.List;
import siasik.entity.Peminjaman;

/**
 *
 * @author ACER
 */
public interface PeminjamanRepository {
     List<Peminjaman> getListPeminjaman();
//     List<Peminjaman> getListPeminjamanById();
     void insertPeminjaman(Peminjaman peminjaman);
     void updateStatusPeminjaman(int idPeminjaman,String status);
     Peminjaman getPeminjamanById(int id);
     boolean getPeminjamanByDate(int idAlatMusik,Date pinjam, Date kembali);
     List<Peminjaman> getListPeminjamanByIdPengguna(int id);
}
