/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package siasik.repository;

import java.util.List;
import siasik.entity.Pengguna;

/**
 *
 * @author ACER
 */
public interface PenggunaRepository {
     List<Pengguna> getListPengguna();
     Pengguna getPenggunaById(int idPengguna);
     void insertPengguna(Pengguna pengguna);
     Pengguna getPenggunaByEmail(String email);
}
