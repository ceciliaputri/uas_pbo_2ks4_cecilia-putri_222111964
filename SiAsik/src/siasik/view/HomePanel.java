/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package siasik.view;

import javax.swing.JScrollPane;

/**
 *
 * @author ACER
 */
public class HomePanel extends javax.swing.JPanel {
    JScrollPane contentScrollPane;

    public HomePanel() {
        initComponents();
    }

    /**
     * Creates new form HomePanel
     */
    public HomePanel(JScrollPane contentScrollPane) {
        this.contentScrollPane = contentScrollPane;
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        loginButton = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        setMaximumSize(new java.awt.Dimension(960, 540));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        loginButton.setBackground(new java.awt.Color(255, 204, 204));
        loginButton.setFont(new java.awt.Font("Javanese Text", 0, 24)); // NOI18N
        loginButton.setText("Login");
        loginButton.setBorderPainted(false);
        loginButton.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        loginButton.setFocusPainted(false);
        loginButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        loginButton.setIconTextGap(5);
        loginButton.setInheritsPopupMenu(true);
        loginButton.setMargin(new java.awt.Insets(2, 14, 2, 14));
        loginButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loginButtonActionPerformed(evt);
            }
        });
        add(loginButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 380, 180, 50));

        jLabel1.setFont(new java.awt.Font("Kristen ITC", 1, 24)); // NOI18N
        jLabel1.setText("Sistem Peminjaman Alat Musik");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 290, -1, -1));

        jLabel3.setFont(new java.awt.Font("Imprint MT Shadow", 1, 100)); // NOI18N
        jLabel3.setText("SiAsik");
        add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 180, 320, 100));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/siasik/view/img/Index.png"))); // NOI18N
        jLabel2.setText("jLabel2");
        jLabel2.setMaximumSize(new java.awt.Dimension(960, 540));
        jLabel2.setMinimumSize(new java.awt.Dimension(960, 540));
        jLabel2.setPreferredSize(new java.awt.Dimension(960, 540));
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 960, 540));
    }// </editor-fold>//GEN-END:initComponents

    private void loginButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loginButtonActionPerformed
        // TODO add your handling code here:
        contentScrollPane.setViewportView(new Login(contentScrollPane));
        
    }//GEN-LAST:event_loginButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JButton loginButton;
    // End of variables declaration//GEN-END:variables
}
